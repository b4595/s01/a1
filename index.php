<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S01-Al</title>
</head>
<body>
	<h1>Full Address</h1>

	<p>
		<?php
			echo getFullAddress('117 Prinza Street ', 'City of General Trias ', 'Cavite ', 'Philippines');
		?>
	</p>

	<p>
		<?php
			echo getFullAddress('123 Mahogany Street ', 'Bamboo City ', 'Kawayan ', 'Philippines');
		?>
	</p>

	<h1>Letter-Based Grading</h1>

	<p>
		<?php
			echo getLetterGrade(99);
		?>
	</p>

	<p>
		<?php
			echo getLetterGrade(90);
		?>
	</p>

	<p>
		<?php
			echo getLetterGrade(76);
		?>
	</p>

</body>
</html>